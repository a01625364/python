# ---------------- 1

# inputFile = open('employees.txt','r')
#
# for line in inputFile:
#     nombre, trabajo, salario = line.split(',')
#     primerNombre, apellido = nombre.split(' ')
#     print('Me llamo ',primerNombre,'. Trabajo en ',trabajo,' y gano $',salario,sep='')
#
# inputFile.close()

# ---------------- 2

# inputFile = open('calificaciones.csv','r')
#
# for line in inputFile:
#     a,b,c,d,e = line.split(',')
#     print('1ro:', a)
#     print('2do:', b)
#
# inputFile.close()

# ---------------- 3

outputFile = open('salida.txt','w')

nombre = input("Dime tu nombre:")
edad = input("Dime tu edad:")

linea = nombre + ' ' + edad + '\n'

outputFile.write(linea)

outputFile.close()