'''
Ej. 1
    Desde el explorador de Windows crear un archivo llamado “Hola.txt”
    Dentro de dicho archivo escribir el texto “Hola mundo”.  Guardarlo.
    Dentro de Python leer el archivo y mostrar el contenido en pantalla
'''

inputFile = open('Hola.txt','w')

for line in inputFile:
    nombre, trabajo, salario = line.split(',')
    primerNombre, apellido = nombre.split(' ')
    print('Me llamo ',primerNombre,'. Trabajo en ',trabajo,' y gano $',salario,sep='')

inputFile.close()

