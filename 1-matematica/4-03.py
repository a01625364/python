'''
Una compañía de telefonía celular cobra $0.80 por mensaje, por mega o por minuto.
Realiza un programa que calcule el costo total mensual de un usuario según estos datos.
'''

# Datos solicitados
mensajes = int(input())
megas = float(input())
minutos = float(input())
precio = 0.9

# Datos a Procesar
totalMensajes = precio * mensajes
totalMegas = precio * megas
totalMinutos = precio * minutos
total = totalMegas + totalMensajes + totalMinutos

# Impresión de datos
print('Costo Mensual:',total)

