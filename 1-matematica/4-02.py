'''
Crea un programa que pregunte al usuario su nombre y su edad.
Debe imprimir en pantalla un mensaje indicando el año en que cumplirá 100 años

'''

# Datos solicitados
nombre = input('Ingrese su nombre: ')
edad = int(input('Ingrese su edad: '))

# Calculo de año en el que cumple 100
año = 2019 - edad + 100

# Impresion de resultado
print('Vas a cumplir 100 en el año:',año)