'''
Escribe un programa que reciba dos números y despliegue en pantalla:
    o La suma
    o La resta
    o La multiplicación
'''

# Variable que guarda el valor del usuario y lo convierte a numero entero.
a = int(input())
b = int(input())

# Suma de a y b
suma = a + b
# Resta de a y b
resta = a - b
# Multiplicación de a y b
multiplicacion = a * b

# Impresión de los resultados
print('Suma:',suma)
print('Resta:',resta)
print('Mult:',multiplicacion)

