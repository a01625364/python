'''
Realiza un programa que indique el número de lustros que ha
vivido una persona por medio de su año de nacimiento y el año actual.
'''

# Datos solicitados
añoNacimiento = int(input())
añoActual = int(input())

# Procesamiento de Datos
lustros = (añoActual - añoNacimiento) / 5

# Impresión de Resultados
print(int(lustros))