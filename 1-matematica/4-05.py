'''
En una universidad cada estudiante cursa 4 materias en el semestre.
Desarrolla un programa que pregunte la calificación de cada materia,
calcula el promedio de las 4 materias y muestra el resultado.
'''

# Datos a solicitar
materia1 = int(input())
materia2 = int(input())
materia3 = int(input())
materia4 = int(input())

# Datos a Procesar
promedio = (materia1 + materia2 + materia3 + materia4) / 4

# Impresión de Datos
print('Su promedio es:',promedio)

