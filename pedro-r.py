'''
Cambia valores

A continuación pide un valor y compara cada uno
de los elementos de la matriz con él. Si son
iguales lo sustituye por un asterisco.

Ejemplo 1:
2
2
1
3
1
4
1
----
[['1', '3'], ['1', '4']]
[['', '3'], ['', '4']]

Ejemplo 2:
2
3
a
b
c
4
a
a
a
----
[['a', 'b', 'c'], ['4', 'a', 'a']]
[['', 'b', 'c'], ['4', '', '*']]

Ejemplo 3:
1
3
1
3
5
3
----
[['1', '3', '5']]
[['1', '*', '5']]
'''

def iniciar(fil,col):
    matriz = []
    for i in range(fil):
        matriz.append([])
        for j in range(col):
            matriz[i].append('')
    return matriz

def rellener(matriz):
    for i in range(len(matriz)):
        for j in range(len(matriz[i])):
            matriz[i][j] = int(input(''))
