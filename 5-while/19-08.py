'''
Escriba un programa que pida números pares mientras el usuario indique que quiere seguir
introduciendo números. Para indicar que quiere seguir escribiendo números, el usuario deberá
contestar S o s a la pregunta.
'''

quiere = 's'
contador = 0

while quiere == 's':
    numero = int(input())
    if numero % 2 == 0:
        contador += 1
        quiere = input('Quiere seguir introduciendo numeros? (S/s):')
    else:
        print(numero, 'no es un numero par. Intentelo de nuevo:')

print('Ha escrito',contador,'numeros pares.')