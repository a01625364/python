'''
Escribe un programa que
calcule
la factorial
de un número cualquiera y lo muestre en pantalla.
'''

numero = int(input())
contador = 1
total = 1

while contador < (numero + 1):
    total = total * contador
    contador = contador + 1

print(total)
