# ----------------------23_02---------------------

x = int(input())
y = int(input())

if x > y:
    for i in range(y, x):
        if i % 2 == 0:
            print(i)
    if y % 2 != 0 and x - y == 1:
        print('No hay pares')
if y > x:
    for i in range(x, y):
        if i % 2 == 0:
            print(i)
    if x % 2 != 0 and y - x == 1:
        print('No hay pares')
if y == x:
    print('No hay pares')