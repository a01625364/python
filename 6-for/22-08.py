'''
Escriba un programa que pregunte cuantos números se
van a introducir, pida esos números (que puedan
ser decimales) y calcule su suma.
'''

cantidad = int(input())
suma = 0

for i in range(cantidad):
    numero = float(input())
    suma += numero

print(suma)

