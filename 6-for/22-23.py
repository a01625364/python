'''
Hacer un que pida por teclado dos números, muestre la suma en
pantalla y pregunte al usuario si quiere realizar otra suma.
'''

# 'si' = otra suma, 'no' = cerrar el programa

quiere = 'si'

while quiere == 'si':
    n1 = int(input())
    n2 = int(input())
    print(n1+n2)
    quiere = input('Quiere realizar otra suma? (si/no): ')

