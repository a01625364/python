

# Variable que guarda 'n' números que usuario quiere introducir
cantidad = int(input(''))

# Variable que guarda el 1er número de n números
anterior = int(input(''))

# Establecemos una iteración de 'n' ciclos
for i in range(cantidad):

    # Variable que guarda el siguiente número para comprar con el 1ro (1ra iteración)
    # y con los anteriores (resto de las iteraciones)
    adicional = int(input(''))

    # Condición de que si el número solicitado es menor al anterior
    if adicional < anterior:
        print('Es menor al anterior')

    # Asignamos número en el segundo lugar al primer lugar en preparación para la próxima iteración
    anterior = adicional



