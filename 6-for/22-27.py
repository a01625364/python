num = 1

def factorial(num):
    total = 1
    for i in range(1,num+1):
        total *= i
    return total

while num != -1:
    num = int(input("Numero (-1 para cortar):"))
    print("Factorial:",factorial(num))