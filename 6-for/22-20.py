'''
Hacer un programa que lea n números y determine cuál es el mayor, el menor y la media de los números leídos.
Tip: Solo la primera vez, el primer número ingresado es asignado tanto a la variable mayor como a la menor.
Posteriormente empieza la comparación.
'''

cantidad = int(input())
mayor = menor = total = 0
contador = 0
bandera = False

while contador < cantidad:
    a = int(input())
    total += a
    if bandera == False:
        menor = a
        bandera = True
    if a > mayor:
        mayor = a
    if a < menor:
        menor = a
    contador += 1

media = total / cantidad

print('Mayor:',mayor)
print('Menor:',menor)
print('Media:',media)