
# Número a analizar
num = int(input(''))

# Solo consideramos los números positivos después del 1 (1 no es primo)
if num > 1:

    # Iniciamos una iteración desde el 2 hasta el número ingresado
    for i in range(2, num):

        # Si algún número dentro del rango divide perfectamente al número analizado, se considera
        # que no es primo, porque la definición de primo es que solo es divisible por 1 y por si mismo
        if (num % i) == 0:
            print(num, "is not a prime number")

            # Usamos break dentro de esta condición para salir de las interaciones porque ya vimos
            # que es divisible por algún otro número
            break

    # Esto corre si el break anterior no se ejecuta y el número a analizar solo es divisible por 1 y por si mismo
    else:
        print(num, "is a prime number")

# Si el número no es mayor a 1, no es primo por definición
else:
    print(num, "is not a prime number")

