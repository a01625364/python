'''
Hacer un programa que calcule la serie de Fibonacci.
La serie de Fibonacci es la sucesión de números:
0, 1, 1, 2, 3, 5, 8, 13, 21, 34, ...
Cada número se calcula sumando los dos anteriores a él
'''

# Cantidad de terminos
nterms = int(input("Cantidad de términos:"))


n1 = 0
n2 = 1
count = 0

if nterms <= 0:
   print("Ingrese numero positivo")
elif nterms == 1:
   print(n1)
else:
   while count < nterms:
       print(n1,end=', ')
       nth = n1 + n2
       n1 = n2
       n2 = nth
       count += 1

