'''
14_09
Escribir un programa que, utilizando funciones con parámetros, lea desde el teclado las
unidades y el precio de un artículo, y de acuerdo a las unidades introducidas le haga un
descuento o no (cuando las unidades excedan media docena se aplicará 4% y el 10%
cuando excedan la docena). La salida de la función será la cantidad rebajada.
'''

a = int(input('Ingrese cantidad a comprar: '))
b = float(input('Ingrese precio de de articulo: '))

def totalDescuentos(unidades,precio):
    total = precio * unidades
    if 12 >= unidades > 6:
        total = total * 0.96
    elif unidades > 12:
        total = total * 0.9
    return total

final = totalDescuentos(a,b)
print('$',final)

