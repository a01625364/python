'''
14_08
Escribir un programa que mediante el llamado a una
función resuelva una ecuación cuadrática,
dados los coeficientes del polinomio.
'''
x = float(input('Ingrese a: '))
y = float(input('Ingrese b: '))
z = float(input('Ingrese c: '))

def bhaskara(a,b,c):
    x = - (4 * a * c) + (b ** 2)
    if x < 0:
        print('Raices no pueden ser extraidas')
        exit()
    x = x ** (1/2)
    x1 = (-b + x) / (2 * a)
    x2 = (-b - x) / (2 * a)
    print('x1:',x1,'\nx2:',x2)

bhaskara(x,y,z)

