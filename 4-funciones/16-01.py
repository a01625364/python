'''
Recuerda que un año es bisiesto si es divisible entre 4,
excepto cuando es divisible entre 100. Aquellos años que
son divisibles entre 100 no son bisiestos, a menos que
sean divisibles entre 400.
'''

def es_bisiesto(año):
    a = (año % 4) == 0 and (año % 100) != 0 or (año % 400) == 0
    return a

ao = int(input(''))
print(es_bisiesto(ao))