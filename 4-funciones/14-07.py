'''
14-07
Escribir un programa que mediante funciones obtenga la distancia
entre dos puntos que se encuentran en el plano a partir de la
lectura de sus coordenadas.
ej. (x1,y1) y (x2,y2)
'''

x1 = float(input('Ingrese x1: '))
y1 = float(input('Ingrese y1: '))
x2 = float(input('Ingrese x2: '))
y2 = float(input('Ingrese y2: '))

a = ( x2 - x1 ) ** 2
b = ( y2 - y1 ) ** 2
distancia = (a + b) ** (1/2)

print('Distancia:',distancia)

