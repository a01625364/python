'''
Escribe un programa que reciba del usuario una lista y devuelva otra
con los elementos de la lista original, pero sin elementos duplicados.
El programa funcionará de la siguiente manera:

- Se recibe un número entero correspondiente al número de elementos que el usuario ingresará.
- Si el valor es 0 o negativo se deberá mandar el mensaje “Error”
- Si el valor ingresado es mayor a 0:
    - Se reciben uno a uno y por renglón, los elementos de la lista.
    - Se despliega la lista original
    - Se despliega la lista sin duplicados
'''

a = int(input())
b = []

if a > 0:
    for i in range(a):
        c = input('')
        b.append(c)
    print(b)

    # print(list(dict.fromkeys(b)))

    d = []
    for j in range(len(b)):
        if b[j] not in d:
            d.append(b[j])
    print(d)

else:
    print('Error')


