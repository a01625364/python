'''
Desarrolla el programa ContarParesImpares.py que determine
el total de valores pares e impares de una lista de números
enteros que recibirá en su entrada. Los valores los captura
el usuario uno por uno, después los almacenará en una lista
y posteriormente se analizará la lista para determinar cuantos
valores pares e impares posee.
'''


# Creamos variables para número inicial
# (para que entre en lacondición) y contadores
numero = pares = impares = 0

# Creamos la lista vacía
lista = []

# Creamos iteración while en vez de for porque no sabemos cuantos
# ciclos van a ser en total
while numero != '*':
    # Pedimos el número
    numero = input()
    # Si el valor ingresado no es el símbolo *, lo convertimos a número entero
    if numero != '*':
        # Lo agregamos a la lista
        lista.append(int(numero))

# Analizamos la lista - Variación A
for i in lista:
    # Si es par, sumamos 1 al contador par
    if i % 2 == 0:
        pares += 1
    # Si es impar, sumamos 1 al contador impar
    else:
        impares += 1

# Analizamos la lista - Variación B
# for i in range(len(lista)):
#     if lista[i] % 2 == 0:
#         pares +=1
#     else:
#         impares = impares + 1

# Imprimimos los resultados
print('PARES=', pares, sep='')
print('IMPARES=', impares, sep='')
