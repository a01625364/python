'''
Escriba un programa que permita crear una lista de palabras y que, a continuación, pida una
palabra y diga cuántas veces aparece esa palabra en la lista.
'''

a = int(input('Ingrese cantidad de palabras a escribir:'))
b = []

for i in range(a):
    b.append(input("Introduce la palabra " + str(i+1) + ":"))

c = input('Ingrese palabra a contar:')

d = b.count(c)

print(d)