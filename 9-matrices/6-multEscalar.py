# Matriz 1
m1 = int(input('Ingrese nº filas: '))
n1 = int(input('Ingrese nº columnas: '))
v1 = int(input('Ingrese valor a ingresar: '))

def inicializar(filas,columnas,valor):
    matriz = []
    for i in range(filas):
        matriz.append([])
        for j in range(columnas):
            matriz[i].append(valor)
    return matriz

def imprimir(matriz):
    for i in range(len(matriz)):
        for j in range(len(matriz[i])):
            print(matriz[i][j],end=' ')
        print()

matriz1 = inicializar(m1,n1,v1)

imprimir(matriz1)

def multiplicarEscalar(matriz1):
    matriz2 = []
    escalar = int(input('Ingrese escalar: '))
    for i in range((len(matriz1))):
        matriz2.append([])
        for j in range(len(matriz1[i])):
            matriz2[i].append(matriz1[i][j] * escalar)
    return matriz2

matriz3 = multiplicarEscalar(matriz1)
imprimir(matriz3)
