'''
Ejercicio 3
Escribe una función que recibe un número entero y devuelve una
matriz de acuerdo con el siguiente ejemplo.

Ejemplo:
Suponga que el usuario introduce el número 8, entonces la función
crea, imprime y devuelve la siguiente matriz:

1 1 1 1 1 1 1 1
0 1 1 1 1 1 1 1
0 0 1 1 1 1 1 1
0 0 0 1 1 1 1 1
0 0 0 0 1 1 1 1
0 0 0 0 0 1 1 1
0 0 0 0 0 0 1 1
0 0 0 0 0 0 0 1
'''

a = int(input('Ingrese valor filas y columnas:'))
b = []

# Create
for fila in range(a):
    b.append([])
    for columna in range(a):
        b[fila].append(7)

# print(b)

# Función convierte el triangulo inferior a valores 0
def triangulo(matrix):
    for i in range(len(b)):
        for j in range(len(b[i])):
            if (i >  j):
                print("0", end=" ")
            else:
                print(matrix[i][j], end=" ")
        print(" ")

triangulo(b)