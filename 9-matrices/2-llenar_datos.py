# Matriz 1
m1 = int(input('Ingrese nº filas: '))
n1 = int(input('Ingrese nº columnas: '))
v1 = input('Ingrese valor a ingresar: ')


def inicializar(filas,columnas,valor):
    matriz = []
    for i in range(filas):
        matriz.append([])
        for j in range(columnas):
            matriz[i].append(valor)
    return matriz

def llenarDatos(matriz):
    for i in range(len(matriz)):
        for j in range(len(matriz[i])):
            matriz[i][j] = int(input('Ingrese valor de ['+str(i+1)+']['+str(j+1)+']: '))
    return matriz

def imprimir(matriz):
    for i in range(len(matriz)):
        for j in range(len(matriz[i])):
            print(matriz[i][j],end=' ')
        print()

matriz1 = inicializar(m1,n1,v1)
matriz1 = llenarDatos(matriz1)

imprimir(matriz1)
