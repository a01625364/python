
# Inicialización
a = [[1,2,3],[4,5,6],[7,8,9]]
turnos = jugadorActual = 0
simbolo = 'X'

# Función Mostrar Matriz
def mostrarMatriz():
    for i in range(3):
        for j in range(3):
            print(a[i][j],end=' ')
        print()

def es_numero(x):
    valores = (1, 2, 3, 4, 5, 6, 7, 8, 9)
    return x in valores

def asignar_a_tabla(simbolo, seleccion):
    if seleccion == 1:
        if a[0][0] == 1:
            a[0][0] = simbolo
            return False
        else:
            return True
    elif seleccion == 2:
        if a[0][1] == 2:
            a[0][1] = simbolo
            return False
        else:
            return True
    elif seleccion == 3:
        if a[0][2] == 3:
            a[0][2] = simbolo
            return False
        else:
            return True
    elif seleccion == 4:
        if a[1][0] == 4:
            a[1][0] = simbolo
            return False
        else:
            return True
    elif seleccion == 5:
        if a[1][1] == 5:
            a[1][1] = simbolo
            return False
        else:
            return True
    elif seleccion == 6:
        if a[1][2] == 6:
            a[1][2] = simbolo
            return False
        else:
            return True
    elif seleccion == 7:
        if a[2][0] == 7:
            a[2][0] = simbolo
            return False
        else:
            return True
    elif seleccion == 8:
        if a[2][1] == 8:
            a[2][1] = simbolo
            return False
        else:
            return True
    else:
        if a[2][2] == 9:
            a[2][2] = simbolo
            return False
        else:
            return True

def cambiar_jugador_simbolo(jugadorActual):
    # Cambio jugador
    if jugadorActual == 0:
        jugadorActual = 1
    else:
        jugadorActual = 0
    # Cambio Simbolo
    if jugadorActual == 0:
        simbolo = 'X'
        print('Va', jugador1,':',simbolo)
        print('--------------------')
    else:
        simbolo = 'O'
        print('Va', jugador2, ':', simbolo)
        print('--------------------')
    # Devuelvo Valores Cambiados
    return jugadorActual, simbolo

def hayGanador(valor):
    return (
            # Horizontal
            a[0][0] == a[0][1] == a[0][2] == valor or
            a[1][0] == a[1][1] == a[1][2] == valor or
            a[2][0] == a[2][1] == a[2][2] == valor or
            # Diagonal
            a[0][0] == a[1][1] == a[2][2] == valor or
            a[0][2] == a[1][1] == a[2][0] == valor or
            # Vertical
            a[0][0] == a[1][0] == a[2][0] == valor or
            a[0][1] == a[1][1] == a[2][1] == valor or
            a[0][2] == a[1][2] == a[2][2] == valor
    )

# Principio del juego
jugador1 = input('Ingrese nombre Jugador 1: ')
jugador2 = input('Ingrese nombre Jugador 2: ')
print('--------------------')

# Muestra las posiciones iniciales
print(jugador1,'usara el símbolo X.')
print(jugador2,'usara el símbolo O.')
print('--------------------')
mostrarMatriz()


# Ciclos de turnos
while turnos < 9:
    if hayGanador(simbolo):
        if simbolo == 'X':
            print('Ha ganado',jugador1,'! Felicitaciones!')
            print('--------------------')
            mostrarMatriz()
            break
        if simbolo == 'O':
            print('Ha ganado',jugador2,'! Felicitaciones!')
            print('--------------------')
            mostrarMatriz()
            break
    seleccion = input('Ingrese posición a ocupar: ')
    try:
        seleccion = int(seleccion)
    except:
        None
    if es_numero(seleccion):
        error = asignar_a_tabla(simbolo, seleccion)
        if error == True:
            print('Ya esta ocupada esa posición.')
            continue
        jugadorActual, simbolo = cambiar_jugador_simbolo(jugadorActual)
    else:
        continue1
    mostrarMatriz()
    turnos += 1
    print('Turno:',turnos)
    print('--------------------')
