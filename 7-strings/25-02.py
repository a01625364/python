'''
Ejercicio 2
Escribe una función que reciba dos parámetros: un texto y una letra.
La función deberá imprimir la cadena, pero en cada letra igual de
“letra” deberá escribir asteriscos.
'''

texto = input()
letra = input()
censurado = ''

for i in texto:
    if i == letra:
        censurado += '*'
    else:
        censurado += i

print(censurado)