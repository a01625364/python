'''
Ejercicio 3
Escribe una función que recibe como parámetro un texto.
La función deberá eliminar todos los espacios en blanco
de la cadena original.
'''

a = input()
b = ''
for caracter in a:
    if caracter != ' ':
        b += caracter
print(b)