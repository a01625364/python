'''
Ejercicio 6
Escribe una función que recibe como parámetro una cadena de
texto. La función deberá devolver verdadero si la cadena es
un palíndromo y falso en caso contrario.
'''

a = input()
b = a[::-1]

if a == b:
    print(True)
else:
    print(False)