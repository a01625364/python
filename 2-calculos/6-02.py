'''
Escribe un programa que pida el valor del radio
de una esfera y muestre su área y su volumen.
'''

# Impprtación de librería
import math

# Datos solicitados
radio = float(input())

# Datos a Procesar
area = 4 * math.pi * (radio ** 2)
volumen = (4 * math.pi * (radio ** 3)) / 3

# Impresión de Datos
print('El área de la esfera es:',area)
print('El volumen de la esfera es:',volumen)
