'''
Escribe un programa que pida al usuario los valores a, b y c
y calcule y muestre el área del triángulo usando esta fórmula.
'''

# Datos solicitados
a = float(input('Ingrese lado a:'))
b = float(input('Ingrese lado b:'))
c = float(input('Ingrese lado c:'))

# Datos Procesados
s = (a + b + c) / 2
area = (s * (s-a) * (s-b) * (s-c)) ** (1/2)

# Impesión de Resultados
print('El área del triangulo es:',area)
